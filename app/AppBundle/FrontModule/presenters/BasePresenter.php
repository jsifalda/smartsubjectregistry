<?php

namespace Smart\SubjectRegistry\FrontModule;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \Flame\Application\UI\Presenter
{
	
	/**
	 * @param \Flame\Addons\FlashMessages\IFlashMessageControlFactory $flashMessageControlFactory
	 * @return \Flame\Addons\FlashMessages\FlashMessageControl
	 */
	protected function createComponentFlashMessage(
		\Flame\Addons\FlashMessages\IFlashMessageControlFactory $flashMessageControlFactory)
	{
		return $flashMessageControlFactory->create();
	}

}
