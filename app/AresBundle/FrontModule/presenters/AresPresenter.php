<?php
/**
 * AresPresenter.php
 *
 * @author  Jiří Šifalda <sifalda.jiri@gmail.com>
 * @date    26.03.13
 */

namespace Smart\SubjectRegistry\FrontModule;

class AresPresenter extends BasePresenter
{

	/**
	 * @inject
	 * @var \Nette\Http\Session
	 */
	public $session;

	/**
	 * @inject
	 * @var \Flame\Ares\AresApi
	 */
	public $aresApi;

	public function actionResult()
	{
		$section = $this->session->getSection('registry');
		$this->template->data = \Nette\ArrayHash::from($section->data);
	}

	/**
	 * @param null $id
	 */
	public function actionGeteway($id = null)
	{
		$response = array();
		if(is_array($data = $this->aresApi->loadData($id))){
			foreach($data as $item){
				if($item instanceof \Flame\Ares\Types\Data){
					$response[] = $item->toArray();
				}
			}
		}

		$this->sendJson(array('source' => $response));
	}

	/**
	 * @param \Smart\SubjectRegistry\AresBundle\Forms\SearchForm\ISearchFormFactory $factory
	 * @return \Smart\SubjectRegistry\AresBundle\Forms\SearchForm\SearchForm
	 */
	protected function createComponentSearchForm(
		\Smart\SubjectRegistry\AresBundle\Forms\SearchForm\ISearchFormFactory $factory)
	{
		$form = $factory->create();
		$form->onSuccess[] = $this->lazyLink('result');
		return $form;
	}

}
