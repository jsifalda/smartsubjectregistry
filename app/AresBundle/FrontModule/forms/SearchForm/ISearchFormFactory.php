<?php
/**
 * ISearchFormFactory.php
 *
 * @author  Jiří Šifalda <sifalda.jiri@gmail.com>
 * @date    26.03.13
 */

namespace Smart\SubjectRegistry\AresBundle\Forms\SearchForm;

interface ISearchFormFactory
{

	/**
	 * @return SearchForm
	 */
	public function create();

}
