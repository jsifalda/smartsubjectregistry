<?php
/**
 * SearchForm.php
 *
 * @author  Jiří Šifalda <sifalda.jiri@gmail.com>
 * @date    26.03.13
 */

namespace Smart\SubjectRegistry\AresBundle\Forms\SearchForm;

class SearchForm extends \Smart\SubjectRegistry\AppBundle\Application\UI\Form
{

	/** @var \Nette\Http\Session */
	private $session;

	/** @var \Flame\Ares\AresApi */
	private $aresApi;

	/** @var \Flame\Ares\Types\IdentificationNumber */
	private $identificationNumber;

	/**
	 * @param \Flame\Ares\Types\IdentificationNumber $identificationNumber
	 */
	public function injectIdentificationNumber(\Flame\Ares\Types\IdentificationNumber $identificationNumber)
	{
		$this->identificationNumber = $identificationNumber;
	}

	/**
	 * @param \Flame\Ares\AresApi $aresApi
	 */
	public function injectAresApi(\Flame\Ares\AresApi $aresApi)
	{
		$this->aresApi = $aresApi;
	}

	/**
	 * @param \Nette\Http\Session $session
	 */
	public function injectSession(\Nette\Http\Session $session)
	{
		$this->session = $session;
	}

	public function __construct()
	{
		parent::__construct();

		$this->configure();

		$this->onSuccess[] = $this->formSubmitted;
	}

	/**
	 * @param SearchForm $form
	 */
	public function formSubmitted(SearchForm $form)
	{

		$values = $form->getValues();

		if(!$this->identificationNumber->verify($values->id)){
			$form->addError('Zadaný výraz není validní IČ.');
			return;
		}

		try {
			$section = $this->session->getSection('registry');
			$section->setExpiration('+ 1 hours');
			$section->data = $this->aresApi->loadData($values->id)->toArray();
		}catch (\Flame\Ares\AresException $ex){
			$form->addError($ex->getMessage());
		}

	}

	private function configure()
	{
		$this->addText('name', 'Název');

		$this->addText('id', 'Identifikační číslo')
			->setOption('description', 'Vyhledávání podle IČ firmy nebo osoby.')
			->setRequired();

		$this->addSubmit('send', 'Hledat')
			->setAttribute('class', 'btn-primary');
	}

}
