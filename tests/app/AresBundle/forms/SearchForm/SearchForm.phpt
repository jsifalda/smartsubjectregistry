<?php
/**
 * Test: Smart\SubjectRegistry\Tests\AresBundle\Form\SearchForm\SearchForm
 *
 * @testCase Smart\SubjectRegistry\Tests\AresBundle\Form\SearchForm\SearchFormTest
 * @author Jiří Šifalda <sifalda.jiri@gmail.com>
 * @package Smart\SubjectRegistry\Tests\AresBundle\Form\SearchForm
 */
namespace Smart\SubjectRegistry\Tests\AresBundle\Form\SearchForm;

use Tester\Assert;

$container = require_once __DIR__ . '/../../../bootstrap.php';

class SearchFormTest extends \Flame\Tests\ApplicationTestCase
{

	/** @var \Smart\SubjectRegistry\AresBundle\Forms\SearchForm\SearchForm */
	private $searchForm;

	public function setUp()
	{
		parent::setUp();

		$this->searchForm = new \Smart\SubjectRegistry\AresBundle\Forms\SearchForm\SearchForm;
	}

	public function testRender()
	{
		$form = new \Flame\Application\UI\Form;
		$form->setRenderer(new \Kdyby\BootstrapFormRenderer\BootstrapRenderer);
		$form->addText('name', 'Název');
		$form->addText('id', 'Identifikační číslo')
			->setRequired()
			->setOption('description', 'Vyhledávání podle IČ firmy nebo osoby.');
		$form->addSubmit('send', 'Hledat')
			->setAttribute('class', 'btn-primary');

		$expected = $this->renderFormTemplate($form);
		Assert::same($expected, $this->renderFormTemplate($this->searchForm));

	}

	public function testSubmittedForm()
	{
		$sessionMock = $this->mockista->create('\Nette\Http\Session');
		$sessionSectionMock = $this->mockista->create('\Nette\Http\SessionSection');
		$sessionSectionMock->expects('setExpiration')
			->with('+ 1 hours')
			->once();
		$sessionSectionMock->expects('__set');
		$sessionMock->expects('getSection')
			->with('registry')
			->once()
			->andReturn($sessionSectionMock);

		$this->searchForm->injectSession($sessionMock);

		$aresDataMock = $this->mockista->create('\Flame\Ares\Types\Data');
		$aresDataMock->expects('toArray')
			->once()
			->andReturn(array());
		$aresMock = $this->mockista->create('\Flame\Ares\AresApi');
		$aresMock->expects('loadData')
			->once()
			->andReturn($aresDataMock);

		$this->searchForm->injectAresApi($aresMock);

		$inMock = $this->mockista->create('\Flame\Ares\Types\IdentificationNumber');
		$inMock->expects('verify')
			->once()
			->with('')
			->andReturn(true);
		$this->searchForm->injectIdentificationNumber($inMock);

		$this->searchForm->formSubmitted($this->searchForm);

		Assert::true(count($this->searchForm->getErrors()) == 0);
	}

	public function testFormSubmittedWithWrongIn()
	{
		$inMock = $this->mockista->create('\Flame\Ares\Types\IdentificationNumber');
		$inMock->expects('verify')
			->once()
			->with('')
			->andReturn(false);
		$this->searchForm->injectIdentificationNumber($inMock);

		$this->searchForm->formSubmitted($this->searchForm);

		Assert::true(count($this->searchForm->getErrors()) > 0);

	}
}

run(new SearchFormTest($container));