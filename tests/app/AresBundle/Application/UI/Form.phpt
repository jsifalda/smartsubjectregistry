<?php
/**
 * Test: Smart\SubjectRegistry\Tests\AresBundle\Application\UI\Form
 *
 * @testCase Smart\SubjectRegistry\Tests\AresBundle\Application\UI\FormTest
 * @author Jiří Šifalda <sifalda.jiri@gmail.com>
 * @package Smart\SubjectRegistry\Tests\AresBundle\Application\UI
 */
namespace Smart\SubjectRegistry\Tests\AresBundle\Application\UI;

use Tester\Assert;

$container = require_once __DIR__ . '/../../../bootstrap.php';

class FormTest extends \Flame\Tests\TestCase
{

	public function testBootstrapRenderer()
	{
		$form = new \Smart\SubjectRegistry\AppBundle\Application\UI\Form;
		Assert::true($form->getRenderer() instanceof \Kdyby\BootstrapFormRenderer\BootstrapRenderer);
	}
}

run(new FormTest($container));